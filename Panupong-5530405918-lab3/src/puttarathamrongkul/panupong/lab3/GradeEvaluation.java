package puttarathamrongkul.panupong.lab3;
/**
 * 
 * Panupong Puttarathamrongkul
 * 553040591-8 Sec2
 *
 */


public class GradeEvaluation {
	public static void grade(double score) {
		if(score >= 0 && score < 50){
			System.out.print(" F");
		}
		else if(score >= 50 && score < 60){
			System.out.print(" D");
		}
		else if(score >= 60 && score < 70){
			System.out.print(" C");
		}
		else if(score >= 70 && score < 80){
			System.out.print(" B");
		}
		else
			System.out.print(" A");
	}
	public static void main(String[] args) {
		if(args.length != 1){
			System.err.println("Usage:<score>");
			System.exit(0);
		}
		double score = Double.parseDouble(args[0]);
		if(score >= 0 && score <= 100){
			System.out.print("The grade for score " + score +  " is");
			grade(score);
		}
		else
			System.out.println(score + " is an invalid score. Please enter score in the range 0-100");
	}

}