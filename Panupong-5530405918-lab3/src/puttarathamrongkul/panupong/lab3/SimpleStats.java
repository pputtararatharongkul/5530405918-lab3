	/*
	* This program to learn how to write subroutines
	* Create by Panupong Puttarathamrongkul
	* 553040591-8 Lab2
	*/
package puttarathamrongkul.panupong.lab3;
import java.util.Arrays;
public class SimpleStats {
	public static void main(String[] args) {
		int n = Integer.parseInt(args[0]);
		double a[] = new double[args.length];
		double sum = 0;
		System.out.println("For the input GPAs:");
		if(args.length == n+1) {
			for(int i=1;i<=n;i++){
				a[i] = Double.parseDouble(args[i]);
				System.out.print(a[i]+" ");
				sum += a[i];
			}
			double average = sum/n;
			System.out.println("\nStats:");
			System.out.println("Average is " + average);
		}else {
			System.err.println("Usage:<SimpleStats> <numGPAs> <GPA>..");
			System.exit(0);
		}
		Arrays.sort(a, 1, n-1);
		System.out.println("Min GPA is " + a[1] + " ");
		System.out.print("Max GPA is " + a[n-1]);
	}
}
