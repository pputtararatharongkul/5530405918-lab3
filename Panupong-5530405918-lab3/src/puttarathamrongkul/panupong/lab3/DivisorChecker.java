package puttarathamrongkul.panupong.lab3;
public class DivisorChecker {
	/**
	 * Create by Panupong Puttarathamrongkul
	 * 553040591-8 sec2 
	 */
	private static int d1,d2,min,max;
	static void displayNumbers (){
		for ( int i = min; i <= max ; i++){
		if (i % d1 == 0 || i % d2 != 0 ){
		continue;
		}
		System.out.println(i);
		}
	}
	public static void main(String[] args) {
		if (args.length != 4) {
			System.err.println("DivisorChecker : <divisor 1> <divisor 2> <min number> <max number>");
			System.exit(0);
			}
			d1 = Integer.parseInt(args[0]);
			d2 = Integer.parseInt(args[1]);
			min = Integer.parseInt(args[2]);
			max = Integer.parseInt(args[3]);
			displayNumbers ();
	}
}