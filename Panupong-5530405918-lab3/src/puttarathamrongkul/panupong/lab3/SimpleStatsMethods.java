package puttarathamrongkul.panupong.lab3;
import java.util.Arrays;

/**
* This program to learn how to write selection statements and iteration statements
* Create by Panupong Puttarathamrongkul
* 553040591-8 Lab2
*/
public class SimpleStatsMethods {
	static int numGPAs;
	static double[] nums;
	static void acceptInput(String[] args){
		int n = Integer.parseInt(args[0]);
		System.out.println("For the input GPAs:");
			for(int i=0;i<n;i++){
				nums[i] = Double.parseDouble(args[i+1]);
				System.out.print(nums[i]+" ");
			}
	}
	static void displayStats(double[] args){
		int n = numGPAs;
		double sum = 0;
			for(int i=0;i<n;i++){
				sum += nums[i];
			}
		double average = sum/n;
		System.out.println("\nStats:");
		System.out.println("Average is " + average);
		Arrays.sort(args, 1, n);
		System.out.println("Min GPA is " + nums[1] + " ");
		System.out.print("Max GPA is " + nums[n-1]);
		}
	public static void main(String[] args) {
		if(args.length < 2){
			System.err.println("Usage:<SimpleStats> <numGPAs> <GPA>..");
			System.exit(0);
		}
		numGPAs = Integer.parseInt(args[0]);
		nums = new double[numGPAs];
		acceptInput(args);
		displayStats(nums);
	}

}
