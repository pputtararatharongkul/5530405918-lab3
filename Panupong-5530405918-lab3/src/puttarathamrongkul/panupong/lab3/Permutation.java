package puttarathamrongkul.panupong.lab3;

public class Permutation {

	static long fact(int n) {
		if(n == 0)
			return 1;
		else
			return n*fact(n-1);
		
	}
	static long permute(int n, int k) {
		long result = fact(n)/fact(n-k);
		return result;
		
	}
	public static void main(String[] args) {
		if(args.length != 2) {
			System.err.println("Usage:Permutation <n> <k>");
			System.exit(1);
		}
		int n = Integer.parseInt(args[0]);
		int k = Integer.parseInt(args[1]);
		long result = permute(n, k);
		System.out.println(n + "! = " + fact(n));
		System.out.println("(" + n + "-" + k + ")! = " + fact(n-k));
		System.out.println(n + "!/(" + n + "-" + k + ")! = " + result);
	}

}
